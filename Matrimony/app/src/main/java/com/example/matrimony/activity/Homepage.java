package com.example.matrimony.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.example.matrimony.R;
import com.example.matrimony.database.MyDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Homepage extends BaseActivity {

    @BindView(R.id.cvActRegistration)
    CardView cvActRegistration;
    @BindView(R.id.cvActList)
    CardView cvActList;
    @BindView(R.id.cvActFavorite)
    CardView cvActFavorite;
    @BindView(R.id.cvActSearch)
    CardView cvActSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_dashboard), false);
        new MyDatabase(this).getWritableDatabase();
    }

    @OnClick(R.id.cvActRegistration)
    public void onCvActRegistrationClicked() {
        Intent intent = new Intent(this, AddUserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActList)
    public void onCvActListClicked() {
        Intent intent = new Intent(this, UserListByGenderActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActFavorite)
    public void onCvActFavoriteClicked() {
        Intent intent = new Intent(this, UserListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActSearch)
    public void onCvActSearchClicked() {
        Intent intent = new Intent(this, ActivitySearchUser.class);
        startActivity(intent);
    }
}