package com.example.matrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony.model.HobbyModel;

import java.util.ArrayList;

public class TblMstHobby extends MyDatabase {

    public static final String TABLE_NAME = "TblMstHobby";
    public static final String HOBBY_ID = "HobbyID";
    public static final String NAME = "Name";

    public TblMstHobby(Context context) {
        super(context);
    }

    public ArrayList<HobbyModel> getHobbyList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<HobbyModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++){
            HobbyModel hobbyModel = new HobbyModel();
            hobbyModel.setHobbyID(cursor.getInt(cursor.getColumnIndex(HOBBY_ID)));
            hobbyModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            list.add(hobbyModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
}
