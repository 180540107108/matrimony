package com.example.matrimony.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.R;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySearchUser extends BaseActivity {

    @BindView(R.id.etSearchUser)
    EditText etUserSearch;
    @BindView(R.id.rcvUsers)
    RecyclerView rcvUsers;

    ArrayList<UserModel> userList = new ArrayList<>();
    ArrayList<UserModel> tempUserList = new ArrayList<>();
    UserListAdapter adapter;
    UserListAdapter.OnViewClickListener onViewClickListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_search_user), true);
        setAdapter();
        setSearchUser();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Sriracha-Regular.ttf");
        etUserSearch.setTypeface(typeface);
    }


    void resetAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public interface OnViewClickListener {
        void onDeleteClick(int position);

        void onFavoriteClick(int position);

        void onItemClick(int position);
    }

    void setSearchUser() {
        etUserSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tempUserList.clear();
                if (charSequence.toString().length() > 0) {
                    for (int j = 0; j < userList.size(); j++) {
                        if (userList.get(j).getName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getFatherName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getSurname().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getEmail().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getPhoneNumber().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            tempUserList.add(userList.get(j));
                        }
                    }
                }
                if (charSequence.toString().length() == 0 && tempUserList.size() == 0) {
                    tempUserList.addAll(userList);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    void setAdapter() {
        rcvUsers.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getUserList());
        tempUserList.addAll(userList);
        adapter = new UserListAdapter(this, tempUserList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void onDeleteClick(int position) {
                if (onViewClickListener != null){
                    onViewClickListener.onDeleteClick(position);
                }

            }

            @Override
            public void onFavoriteClick(int position) {

            }

            @Override
            public void onItemClick(int position) {
                if (onViewClickListener != null){
                    onViewClickListener.onItemClick(position);
                }

            }
        });
        rcvUsers.setAdapter(adapter);
    }
}
