package com.example.matrimony.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimony.R;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.TblUser;
import com.example.matrimony.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends BaseActivity {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;
    @BindView(R.id.noFavorites)
    TextView noFavorites;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_user_list), true);
        setAdapter();
    }

    void setAdapter(){
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getFavoriteUserList());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void onDeleteClick(int position) {

            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUserID = new TblUser(UserListActivity.this).updateFavoriteStatus(0,
                        userList.get(position).getUserId());
                if (lastUpdatedUserID > 0) {
                    userList.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(0, userList.size());
                }
            }

            @Override
            public void onItemClick(int position) {

            }
        });
        rcvUserList.setAdapter(adapter);
        checkAndVisibleView();
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            noFavorites.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            noFavorites.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }
}
