package com.example.matrimony.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.matrimony.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    Button btnStart;
    @BindView(R.id.btnActStart)
    Button btnActStart;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        btnStart = findViewById(R.id.btnActStart);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(new Intent(MainActivity.this, Homepage.class));
                startActivity(intent);
            }
        });
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/EmilysCandy-Regular.ttf");
        btnActStart.setTypeface(typeface);

    }

}