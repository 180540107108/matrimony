package com.example.matrimony.model;

import java.io.Serializable;

public class HobbyModel implements Serializable {

    int HobbyID;
    String name;

    public int getHobbyID() {
        return HobbyID;
    }

    public void setHobbyID(int hobbyID) {
        HobbyID = hobbyID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
